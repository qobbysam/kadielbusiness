'use strict';

var HomeCtrl = function(){

};

var MessageHomeCtrl = function(){

};

var MessageListHomeCtrl = function(){

};

var MessageThreadReadCtrl = function(){

};

var ProductListHomeCtrl = function(){

};

var  ProductSpecificCtrl = function(){

};

var ProductCreateCtrl = function(){

};

var ProductCreateAddImagesCtrl = function(){

};

var ProductPreviewCtrl = function(){

};

var ProductEditCtrl = function(){

};

var  ProductStatsHomeCtrl= function(){

};

var ProductStatsSpecificCtrl = function(){

};

var  ConsumerStatsHomeCtrl = function(){

};

var ConsumerStatsSpecificCtrl  = function(){

};

var CheckoutCtrl = function(){

};

var  ManagementHomeCtrl = function(){

};

var ManagementSpecificCtrl = function(){

};

var SuperManageHomeCtrl = function($scope, $state,SuperManageHomeService){
  $state.reload();
 var list= SuperManageHomeService.return_all()
  // .then(function(e){
  //   console.log(e);
  // })

  // .catch(function(e){
  //   console.log(e);
  // })
  $scope.employees = list;
  $scope.do_edit = function(val){

    console.log(val);

  }

  $scope.add_employee = function(){
    $state.go('app.supermanageAdd')
  }
  $scope.remove_employee = function(id_one){
    $state.go('app.supermanageRemove',{ id:id_one});
  }

console.log(list);



};

var SuperManageAddCtrl = function(){

};

var SuperManageRemoveCtrl= function($scope, $state, SuperManageRemoveService){
  var remove = function(id){
    SuperManageRemoveService.remove_one(id);

    setTimeout(function(){$state.go('app.supermanage')},5000 );
    
  }

  console.log($state.params)

  remove($state.params.id)

};

var SettingsCtrl = function(){

};




angular.module('starter.controllers', [])

.controller('HomeCtrl',HomeCtrl )
.controller('MessageHomeCtrl', MessageHomeCtrl)
.controller('MessageListHomeCtrl', MessageListHomeCtrl)
.controller('MessageThreadReadCtrl', MessageThreadReadCtrl)
.controller('ProductListHomeCtrl', ProductListHomeCtrl)
.controller('ProductSpecificCtrl',ProductSpecificCtrl )
.controller('ProductCreateCtrl', ProductCreateCtrl)
.controller('ProductCreateAddImagesCtrl',ProductCreateAddImagesCtrl )
.controller('ProductPreviewCtrl',ProductPreviewCtrl )
.controller('ProductEditCtrl',ProductEditCtrl )
.controller('ProductStatsHomeCtrl', ProductStatsHomeCtrl)
.controller('ProductStatsSpecificCtrl', ProductStatsSpecificCtrl)
.controller('ConsumerStatsHomeCtrl',ConsumerStatsHomeCtrl )
.controller('ConsumerStatsSpecificCtrl',ConsumerStatsSpecificCtrl )
.controller('CheckoutCtrl',CheckoutCtrl )
.controller('ManagementHomeCtrl',ManagementHomeCtrl )
.controller('ManagementSpecificCtrl',ManagementSpecificCtrl )
.controller('SuperManageHomeCtrl', SuperManageHomeCtrl)
.controller('SuperManageAddCtrl', SuperManageAddCtrl)
.controller('SuperManageRemoveCtrl',SuperManageRemoveCtrl )
.controller('SettingsCtrl',SettingsCtrl )



.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, controller are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});
