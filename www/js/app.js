// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

        .state('app.home', {
    url: '/home',
    //abstract: true,

    views: {
      'menuContent': {
     
    templateUrl: 'templates/home.html',
    controller: 'HomeCtrl'   
      }
    }
  })


    .state('app.products', {
    url: '/products',
    //abstract: true,

    views: {
      'menuContent': {
     
    templateUrl: 'templates/product.html',
    controller: 'ProductListHomeCtrl'   
      }
    }
  })
    

    .state('app.productSpecicfic', {
      url: '/products/specicfic/:key',
      views: {
        'menuContent': {
          templateUrl: 'templates/product-specific.html',
          controller: 'ProductsSpecificCtrl'
        }
      }
    })


    .state('app.productCreate', {
    url: '/products/create',
    
    views: {
      'menuContent': {
     
    templateUrl: 'templates/product-create.html',
    controller: 'ProductCreateCtrl'   
      }
    }
  })

    .state('app.productCreateAddImages', {
    url: '/products/create/add-images/:key',
    
    views: {
      'menuContent': {
     
    templateUrl: 'templates/product-create-add-images.html',
    controller: 'ProductCreateAddImagesCtrl'   
      }
    }
  })
    
        .state('app.productCreatePreview', {
    url: '/product/preview/:key',
    
    views: {
      'menuContent': {
     
    templateUrl: 'templates/product-preview.html',
    controller: 'ProductPreviewCtrl'   
      }
    }
  })


        //// Product edit


      .state('appProductEdit', {
    url: '/products/edit/',
   
    views: {
      'menuContent': {
     
    templateUrl: 'templates/product-edit.html',
    controller: 'ProductEditCtrl'   
      }
    }
  })

      .state('app.productEditAddImages', {
    url: '/products/edit/add-images/:key',
    
    views: {
      'menuContent': {
     
    templateUrl: 'templates/product-edit-add-images.html',
    controller: 'ProductCreateAddImagesCtrl'   
      }
    }
  })
    
    .state('app.productEditPreview', {
    url: '/product/edit/preview/:key',
    
    views: {
      'menuContent': {
     
    templateUrl: 'templates/product-preview.html',
    controller: 'ProductPreviewCtrl'   
      }
    }
  })

///Product Stats

    .state('app.productStats', {
    url: '/product/stats',
    
    views: {
      'menuContent': {
     
    templateUrl: 'templates/product-stats.html',
    controller: 'ProductStatsHomeCtrl'   
      }
    }
  })


    .state('app.productStatSpecicfic', {
    url: '/product/stats/specicfic/:key',
    
    views: {
      'menuContent': {
     
    templateUrl: 'templates/product-stats-specific.html',
    controller: 'ProductStatsSpecificCtrl'   
      }
    }
  })


//// Consumer Stats    
    .state('app.consumer', {
    url: '/consumer/stats',
    
    views: {
      'menuContent': {
     
    templateUrl: 'templates/consumer-stats.html',
    controller: 'ConsumerStatsHomeCtrl'   
      }
    }
  })
    .state('app.consumerSpecific', {
    url: '/consumer/stats/specific/:key',
    
    views: {
      'menuContent': {
     
    templateUrl: 'templates/consumer-stats-specific.html',
    controller: 'ConsumerStatsSpecificCtrl'   
      }
    }
  })

///Checkout


    .state('app.coupon', {
    url: '/checkout',
    
    views: {
      'menuContent': {
     
    templateUrl: 'templates/coupon-checkout.html',
    controller: 'CheckoutCtrl'   
      }
    }
  })

////Messages


    .state('app.messages', {
    url: '/messages',
    
    views: {
      'menuContent': {
     
    templateUrl: 'templates/messages-home.html',
    controller: ''   
      }
    }
  })
    .state('app.messagesInbox', {
    url: '/messages/inbox',
    
    views: {
      'menuContent': {
     
    templateUrl: 'templates/messages-list.html',
    controller: 'MessageListHomeCtrl'   
      }
    }
  })
    .state('app.messagesOutbox', {
    url: '/messages/outbox',
    
    views: {
      'menuContent': {
     
    templateUrl: 'templates/messages-list.html',
    controller: 'MessageListHomeCtrl'   
      }
    }
  })
    .state('app.messagesThread', {
    url: '/messages/read',
        views: {
      'menuContent': {
          templateUrl: 'templates/messages-thread.html',
    controller: 'MessageThreadReadCtrl'        
      }
    }

  })




    .state('app.manage', {
    url: '/manage',
    
    views: {
      'menuContent': {
     
    templateUrl: 'templates/management-home.html',
    controller: 'ManagementHomeCtrl'   
      }
    }
  })
    .state('app.manageSpecific', {
    url: '/manage/specific',
    
    views: {
      'menuContent': {
    templateUrl: 'templates/management-individual.html',
    controller: 'ManagementSpecificCtrl'    
      }
    }
    
  })



///Super Manage
    .state('app.supermanage', {
    url: '/supermanage',
    
    views: {
      'menuContent': {
    templateUrl: 'templates/supermanage-home.html',
    controller: 'SuperManageHomeCtrl'        
      }
    }

  })
    .state('app.supermanageAdd', {
    url: '/supermanage/add',
    
    views: {
      'menuContent': {
    templateUrl: 'templates/supermanage-add.html',
    controller: 'SuperManageAddCtrl'   
      }
    }

  })
    .state('app.supermanageRemove', {
    url: '/supermanage/remove/:id',
    
    views: {
      'menuContent': {
    templateUrl: 'templates/supermanage-remove.html',
    controller: 'SuperManageRemoveCtrl'        
      }
    }

  })
  

  //   .state('app.supermanage.', {
  //   url: '/supermanage/edit',
    
  //   templateUrl: 'templates/supermanage-edit.html',
  //   controller: ''
  // })



    .state('app.settings', {
    url: '/settings',
    views: {
      'menuContent': {
            templateUrl: 'templates/settings.html',
            controller: 'SettingsCtrl'

      }

    }

  }) 








  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })
    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/playlists');
});




