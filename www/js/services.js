'use strict';

var supermanage_all_employeess = [
{"id":1,"name":"Todd Bailey","pin":8636,"position":1},
{"id":2,"name":"Fred Palmer","pin":2101,"position":3},
{"id":3,"name":"Fred Roberts","pin":8187,"position":2},
{"id":4,"name":"Jason Kelley","pin":5695,"position":1},
{"id":5,"name":"Heather Rodriguez","pin":1470,"position":1},
{"id":6,"name":"Sarah Cruz","pin":7065,"position":2},
{"id":7,"name":"Kenneth Hayes","pin":7258,"position":2},
{"id":8,"name":"Helen Fowler","pin":4495,"position":2},
{"id":9,"name":"Sharon Nichols","pin":7670,"position":2},
{"id":10,"name":"Joseph Ferguson","pin":4402,"position":1},
{"id":11,"name":"Robin Wheeler","pin":4443,"position":1},
{"id":12,"name":"Dorothy Gardner","pin":2215,"position":3},
{"id":13,"name":"Pamela Ryan","pin":5457,"position":3},
{"id":14,"name":"Jeremy Porter","pin":1200,"position":4},
{"id":15,"name":"Jerry Kelly","pin":2283,"position":3},
{"id":16,"name":"Lori Perry","pin":8572,"position":1},
{"id":17,"name":"Earl Moore","pin":3583,"position":3},
{"id":18,"name":"Ralph Warren","pin":2962,"position":1},
{"id":19,"name":"Johnny Montgomery","pin":6163,"position":2},
{"id":20,"name":"Martha Murphy","pin":4197,"position":3}
]




var HomeService = function(){

};

var MessageHomeService = function(){

};

var MessageListHomeService = function(){

};

var MessageThreadReadService = function(){

};

var ProductListHomeService = function(){

};

var  ProductSpecificService = function(){

};

var ProductCreateService = function(){

};

var ProductCreateAddImagesService = function(){

};

var ProductPreviewService = function(){

};

var ProductEditService = function(){

};

var  ProductStatsHomeService= function(){

};

var ProductStatsSpecificService = function(){

};

var  ConsumerStatsHomeService = function(){

};

var ConsumerStatsSpecificService  = function(){

};

var CheckoutService = function(){

};

var  ManagementHomeService = function(){

};

var ManagementSpecificService = function(){

};

var SuperManageHomeService = function(){
	var return_all = function(){
		return supermanage_all_employeess
	}

	return {
		return_all: return_all
	}

	
};

var SuperManageAddService = function(){

};

var SuperManageRemoveService= function(){

	var remove_one = function(id){
		console.log("removing " + id);
		for (var i=0;i<supermanage_all_employeess.length; i++){
			//console.log(i)
			var obj = supermanage_all_employeess[i];
			//console.log(obj);
			if(obj.id == id){
				console.log("removing me");
				console.log(obj)
				supermanage_all_employeess.splice(i,1);
			}
		}

	}
	return {remove_one:remove_one}

};

var SettingsService = function(){

};




angular.module('starter.services', [])

.factory('HomeService',HomeService )
.factory('MessageHomeService', MessageHomeService)
.factory('MessageListHomeService', MessageListHomeService)
.factory('MessageThreadReadService', MessageThreadReadService)
.factory('ProductListHomeService', ProductListHomeService)
.factory('ProductSpecificService',ProductSpecificService )
.factory('ProductCreateService', ProductCreateService)
.factory('ProductCreateAddImagesService',ProductCreateAddImagesService )
.factory('ProductPreviewService',ProductPreviewService )
.factory('ProductEditService',ProductEditService )
.factory('ProductStatsHomeService', ProductStatsHomeService)
.factory('ProductStatsSpecificService', ProductStatsSpecificService)
.factory('ConsumerStatsHomeService',ConsumerStatsHomeService )
.factory('ConsumerStatsSpecificService',ConsumerStatsSpecificService )
.factory('CheckoutService',CheckoutService )
.factory('ManagementHomeService',ManagementHomeService )
.factory('ManagementSpecificService',ManagementSpecificService )
.factory('SuperManageHomeService', SuperManageHomeService)
.factory('SuperManageAddService', SuperManageAddService)
.factory('SuperManageRemoveService',SuperManageRemoveService )
.factory('SettingsService',SettingsService );
